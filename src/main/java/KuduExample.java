import com.opencsv.CSVReader;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.apache.kudu.ColumnSchema;
import org.apache.kudu.Schema;
import org.apache.kudu.Type;
import org.apache.kudu.client.*;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class KuduExample {

    private static final String KUDU_MASTERS = System.getProperty("kuduMasters", "192.168.5.192:7051");
    private static final String FILE_PATH = "/home/nishat/voter_file.csv";
    private static final String TABLE_NAME = "impala::default.voters";

    static void createVoterTable(KuduClient client) throws KuduException {

        String tableName = "voter_table";

        List<ColumnSchema> columns = new ArrayList<>();

        columns.add(new ColumnSchema.ColumnSchemaBuilder("voter_id", Type.STRING)
                .key(true)
                .build());

        columns.add(new ColumnSchema.ColumnSchemaBuilder("voter_no", Type.STRING)
                .key(true)
                .build());

        columns.add(new ColumnSchema.ColumnSchemaBuilder("name", Type.STRING)
                .nullable(true)
                .build());

        columns.add(new ColumnSchema.ColumnSchemaBuilder("dateOfBirth", Type.STRING)
                .nullable(true)
                .build());

        columns.add(new ColumnSchema.ColumnSchemaBuilder("nid", Type.STRING)
                .nullable(true)
                .build());

        columns.add(new ColumnSchema.ColumnSchemaBuilder("city", Type.STRING)
                .nullable(true)
                .build());

        Schema schema = new Schema(columns);
        CreateTableOptions cto = new CreateTableOptions();
        List<String> hashKeys = new ArrayList<>(1);
        hashKeys.add("voter_id");
        int numBuckets = 3;
        cto.addHashPartitions(hashKeys, numBuckets);
        cto.setNumReplicas(1);
        client.createTable(tableName, schema, cto);
        System.out.println("Created table " + tableName);
    }

    static void scanVoterTable(KuduClient client) throws KuduException {
        KuduTable table = client.openTable("impala::default.voters");
        Schema schema = table.getSchema();

        List<String> projectColumns = new ArrayList<>();
        projectColumns.add("voter_no");
        projectColumns.add("name");
        projectColumns.add("nid");
        projectColumns.add("city");
        int lowerBound = 0;

        KuduPredicate byCity = KuduPredicate.newComparisonPredicate(
                schema.getColumn("city"),
                KuduPredicate.ComparisonOp.EQUAL, "Dhaka_1");

        KuduScanner scanner = client.newScannerBuilder(table)
                .setProjectedColumnNames(projectColumns)
//                .addPredicate(byCity)
                .build();

        while (scanner.hasMoreRows()) {
            RowResultIterator results = scanner.nextRows();
            while (results.hasNext()) {
                RowResult result = results.next();
                System.out.println(result.getString("name"));
                System.out.println(result.getString("nid"));
                System.out.println(result.getString("city"));
            }
        }
        System.out.println("Scanned some rows and checked the results");
    }

    static void insertVoterRows(List<String> records, KuduSession session, KuduTable table) throws KuduException {

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        System.out.println("Start inserting data");
        Insert insert = table.newInsert();
        PartialRow row = insert.getRow();
        row.addString("voter_id", records.get(0));
        row.addString("voter_no", records.get(1));
        row.addString("name", records.get(2));
        row.addString("nid", records.get(3));
        row.addString("dateofbirth", records.get(4));
        row.addString("city", records.get(5));
        session.apply(insert);

        System.out.println("Single row insert time " + stopWatch.getTime() + "ms");
        stopWatch.stop();

        session.close();
        if (session.countPendingErrors() != 0) {
            System.out.println("errors inserting rows");
            org.apache.kudu.client.RowErrorsAndOverflowStatus roStatus = session.getPendingErrors();
            org.apache.kudu.client.RowError[] errs = roStatus.getRowErrors();
            int numErrs = Math.min(errs.length, 5);
            System.out.println("there were errors inserting rows to Kudu");
            System.out.println("the first few errors follow:");
            for (int i = 0; i < numErrs; i++) {
                System.out.println(errs[i]);
            }
            if (roStatus.isOverflowed()) {
                System.out.println("error buffer overflowed: some errors were discarded");
            }
            throw new RuntimeException("error inserting rows to Kudu");
        }
//        System.out.println("Inserted " + records.getClass() + " rows");
    }

    public static void main(String[] args) throws KuduException {

        System.out.println("-----------------------------------------------");
        System.out.println("Will try to connect to Kudu master(s) at " + KUDU_MASTERS);
        System.out.println("Run with -DkuduMasters=master-0:port,master-1:port,... to override.");
        System.out.println("-----------------------------------------------");
        KuduClient client = new KuduClient.KuduClientBuilder(KUDU_MASTERS).build();

//        Scanner input = new Scanner(System.in);
//        String filePath, tableName;
//        do {
//            System.out.println("Enter File Path");
//            filePath = input.nextLine();
//        } while (StringUtils.isEmpty(filePath));
//
//        do {
//            System.out.println("Enter Table Name");
//            tableName = input.nextLine();
//        } while (StringUtils.isEmpty(tableName));

//        System.out.println(filePath);
//        System.out.println(tableName);

        KuduTable table = client.openTable(TABLE_NAME);
        KuduSession session = client.newSession();
//        session.setMutationBufferSpace();
        session.setFlushMode(SessionConfiguration.FlushMode.AUTO_FLUSH_BACKGROUND);

        try (CSVReader csvReader = new CSVReader(new FileReader(FILE_PATH))) {
            String[] values;
            StopWatch stopWatch = new StopWatch();
            System.out.println("Start Processing data at " + stopWatch.getTime());
            stopWatch.start();
            while ((values = csvReader.readNext()) != null) {
                insertVoterRows(Arrays.asList(values), session, table);
            }
            System.out.println("Total Processing time " + stopWatch.getTime() + " ms");
            stopWatch.stop();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                client.shutdown();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
