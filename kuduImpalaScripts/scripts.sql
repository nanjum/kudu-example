-- ## TABLE Create

CREATE TABLE voters
(
  voter_id    STRING,
  voter_no    STRING,
  name        STRING,
  dateOfBirth STRING,
  nid         STRING,
  city        STRING,
  PRIMARY KEY (voter_id)
)
  PARTITION BY HASH PARTITIONS 16
  STORED AS KUDU
  TBLPROPERTIES (
  'kudu.master_addresses' = '192.168.5.192:7051',
  'kudu.num_tablet_replicas' = '1'
);

-- Drop Table
DROP TABLE voters;

-- Count Query

select
  city,
  count(voter_no) as no_of_voter
from voters
group by city
order by city desc
limit 5;